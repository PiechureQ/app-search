package com.app.piechureq.appsearch;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CustomAdapter extends ArrayAdapter<ListItems>{//adapter ładujący ikony i nazwy aplikacji do Listy w MainActivity

    private final Activity context;

    private final List<ListItems> items;
    private final List<ListItems> itemsDef;

    public CustomAdapter(Activity context, List<ListItems> items){
        super(context, R.layout.activity_listview, items);
        this.context = context;
        this.items = items;

        this.itemsDef = new ArrayList<ListItems>(items);
    }

   // private boolean compare (View view1, String string){
   //     return view1.equals(string);
   // }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.activity_listview, null, true);
        TextView txtView = (TextView) rowView.findViewById(R.id.app_label);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.app_icon);

        txtView.setText(items.get(position).getName());
        imageView.setImageDrawable(items.get(position).getImage());


        //compare(txtView, items.get(position).getName());
        //Collections.sort((List<Comparable>) txtView.getText());

        return rowView;
    }

    @Override
    public void sort(@NonNull Comparator<? super ListItems> comparator) {
        super.sort(comparator);
    }

    @Override
    public Filter getFilter(){
        return new Filter(){

            @Override
            protected FilterResults performFiltering(CharSequence query) {
                query = query.toString().toLowerCase();
                FilterResults result = new FilterResults();

                if (query != null && query.toString().length() > 0) {
                    List<ListItems> foundedData = new ArrayList<ListItems>();
                    for(ListItems item : itemsDef){
                        if(item.getName().toString().toLowerCase().contains(query)){
                            foundedData.add(item);
                        }
                    }
                    result.values = foundedData;
                    result.count = foundedData.size();
                }else {
                    result.values = itemsDef;
                    result.count = itemsDef.size();
                }

                return result;
            }
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                clear();
                for (ListItems item : (List<ListItems>) results.values) {
                    add(item);
                }
                notifyDataSetChanged();
            }

        };
    }


}
