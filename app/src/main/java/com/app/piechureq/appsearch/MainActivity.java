package com.app.piechureq.appsearch;

import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<ListItems> arraylist = new ArrayList<ListItems>();

    private List<String> getApplications(){//zapisuje do listy aplikacje
        final PackageManager pm = getPackageManager();

        List<String> applications = new ArrayList<String>();

        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> list = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        for (ResolveInfo rInfo : list) {
            applications.add(rInfo.activityInfo.applicationInfo.loadLabel(pm).toString());

            Log.w("Installed Applications", rInfo.activityInfo.applicationInfo.packageName);

        }
        return applications;
    }

    private List<Drawable> getIcons() throws PackageManager.NameNotFoundException {//zapisuje do listy ikony wskazanych aplikacji
        final PackageManager pm = getPackageManager();

        List<Drawable> applicationIcon = new ArrayList<Drawable>();

        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> list = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo rInfo : list) {
            applicationIcon.add(rInfo.activityInfo.applicationInfo.loadIcon(pm));

        }
        return applicationIcon;
    }

    private List<String> getPackagesName(){//zapisuje do listy nazwy pakietów do uruchomienia aplikacji
        final PackageManager pm = getPackageManager();

        List<String> packagesName = new ArrayList<String>();

        Intent intent = new Intent(Intent.ACTION_MAIN, null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> list = pm.queryIntentActivities(intent, PackageManager.PERMISSION_GRANTED);
        for (ResolveInfo rInfo : list) {
            packagesName.add(rInfo.activityInfo.packageName);

            Log.w("Installed Applications", rInfo.activityInfo.packageName);
        }
        return packagesName;
    }

    protected void launchApp(String packageName) {//uruchamia wskazany pakiet

        Intent intent = getPackageManager().getLaunchIntentForPackage(packageName);

        if (intent != null) {
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException err) {
                Toast t = Toast.makeText(getApplicationContext(),
                        R.string.app_not_found,
                        Toast.LENGTH_SHORT);
                t.show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ustawienia wyszukiwania
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) findViewById(R.id.Search);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        //tworzy liste nazw i icon do ListItems
        List<String> names = getApplications();

        List <Drawable> icons = null;
        try {
            icons = getIcons();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //Collections.sort(names);
        //wszytskie elementy do jednej listy
        for (int i = 0; i < names.size(); i++){
            ListItems item = new ListItems(names.get(i), icons.get(i));

            arraylist.add(item);
        }



        //ustawianie zawartości listy
        final CustomAdapter appArray = new CustomAdapter(MainActivity.this, arraylist);
        final ListView appList = (ListView) findViewById(R.id.AppList);
        appList.setAdapter(appArray);

        //uruchamia przy kliknieciu
        appList.setOnItemClickListener(new AdapterView.OnItemClickListener() {//uruchamia kliknieta aplikacje z listy
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view.findViewById(R.id.app_label);
                String appName = String.valueOf(getPackagesName().get(getApplications().indexOf(textView.getText())));

                launchApp(appName);
            }
        });

        //filter on input
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {//przeszukuje liste przy każdej zmianie
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                appArray.getFilter().filter(query);

                return true;
            }
        });
    }
}
