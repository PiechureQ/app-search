package com.app.piechureq.appsearch;

import android.graphics.drawable.Drawable;

/**
 * Created by xmichalx on 2017-03-05.
 */

public class ListItems {
    private String name;
    private Drawable image;

    public ListItems(String name, Drawable image){
        this.name = name;
        this.image = image;
    }

    public String getName(){
        return this.name;
    }
    public Drawable getImage(){
        return this.image;
    }
}
